odoo.define('service_activities.compartment_list', function (require) {

        "use strict";
    var core = require('web.core');
    var formats = require('web.formats');
    var Model = require('web.Model');
    var session = require('web.session');
    var ajax = require('web.ajax');
    var KanbanView = require('web_kanban.KanbanView');
    var KanbanRecord = require('web_kanban.Record');
    var ActionManager = require('web.ActionManager');
    var QWeb = core.qweb;

    var _t = core._t,
        _lt = core._lt;
    // var QWeb = instance.web.qweb;



    // local.CompartmentPage = instance.Widget.extend({
    // 	template: "CompartmentTemplate",
    // 	start: function() {
    //   	},
    // });

    var CompartmentListView = KanbanView.extend({
        display_name: _lt('CompartmentList'),
        searchview_hidden: true,//  To hide the search and filter bar

        init: function (parent, dataset, view_id, options) {
            this._super(parent, dataset, view_id, options);
            this.options.creatable = false;
            var uid = dataset.context.uid;
            var client_data = true;
            var isFirefox = false;
            console.log(this)
            //Here we can bind any functions to be called before or after render.
            //_.bindAll(this, 'render', 'graph');
            //var _this = this;
            //this.render = _.wrap(this.render, function(render) {
            //    render();
            //    _this.graph();
            //    return _this;
            //});
        },

        fetch_data: function() {
            // Overwrite this function with useful data
            return $.when();
        },     

        render: function() {
            var super_render = this._super;
            var self = this;
            var model  = new Model('funeral_home.observation').call('get_deceased_info').then(function(result){
                self.isFirefox = typeof InstallTrigger !== 'undefined';
                self.client_data =  result[0]
                return self.fetch_data().then(function(result){
                    var client_list = QWeb.render('service_activities.compartment_list', {
                        widget: self,
                    });
                    super_render.call(self);
                    $(hr_dashboard).prependTo(self.$el);
                })
            });
        },

        dataTable: function(){
            $('#client_details').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel',
                {
                    extend: 'pdf',
                    footer: 'true',
                    orientation: 'landscape',
                    title:'Employee Details',
                    text: 'PDF',
                    exportOptions: {
                        modifier: {
                            selected: true
                        }
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                    columns: ':visible'
                    }
                },
            'colvis'
            ],
            columnDefs: [ {
                targets: -1,
                visible: false
            } ],
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            pageLength: 15,
            } );
        },

    	start: function() {
      	}
    })

// View adding to the registry
core.view_registry.add('compartment_list_view', CompartmentListView);
return CompartmentListView
});

