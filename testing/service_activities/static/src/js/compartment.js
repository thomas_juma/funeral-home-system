
openerp.service_activities = function(instance, local) {
    
        "use strict";
    var core = instance.web.core;
    var formats = instance.web.formats;
    var Model = instance.web.Model;
    var session = instance.web.session;
    var ajax = instance.web.ajax;
    // var KanbanView = instance.web_kanban.KanbanView;
    // var KanbanRecord = instance.web_kanban.Record;
    var ActionManager = instance.web.ActionManager;

    var _t = instance.web._t,
        _lt = instance.web._lt;
    var QWeb = instance.web.qweb;



    local.CompartmentPage = instance.Widget.extend({
    	template: "CompartmentTemplate",
    	start: function() {

      	},
    });

    instance.web.client_actions.add('service_activities.compartment', 'instance.service_activities.CompartmentPage');



    local.CompartmentListPage = instance.Widget.extend({
    	template: "CompartmentListTemplate",
    	start: function () {
            var self = this;
            return new instance.web.Model('funeral_home.observation')
                .query(['fh_admission_tag_no', 
                        'fh_name_of_deceased', 
                        'fh_client_sex', 
                        'date_fh_order', 
                        'date_fh_of_birth', 
                        'fh_postmortem_interval', 
                        'fh_forensic_case', 
                        'fh_preservation', 
                        'fh_clothed', 
                        'fh_stay_period',
                        'fh_reconstruction',
                        'fh_cosmetics'])
                .all()
                .then(function (results) {
                    console.log(results);
                    self.isFirefox = typeof InstallTrigger !== 'undefined';
                    self.client_data = results[0]
                    self.$('#client_details').DataTable( {
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel',
                            {
                                extend: 'pdf',
                                footer: 'true',
                                orientation: 'landscape',
                                title:'Client Details',
                                text: 'PDF',
                                exportOptions: {
                                    modifier: {
                                        selected: true
                                    }
                                }
                            },
                            {
                                extend: 'print',
                                exportOptions: {
                                columns: ':visible'
                                }
                            },
                        'colvis'
                        ],
                        data: results,
                        columns: [
                            { "data": "fh_admission_tag_no" },
                            { "data": "fh_name_of_deceased" },
                            { "data": "fh_client_sex" },
                            { "data": "date_fh_order" },
                            { "data": "date_fh_of_birth"},
                            { "data": "fh_postmortem_interval"},
                            { "data": "fh_forensic_case"},
                            { "data": "fh_preservation"},
                            { "data": "fh_clothed"},
                            { "data": "fh_stay_period"},
                            { "data": "fh_reconstruction"},
                            { "data": "fh_cosmetics"}
                           ],
                        columnDefs: [ {
                            targets: -1,
                            visible: false
                        } ],
                        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        pageLength: 15,
                    } );
                    // self.$("#client_details tbody").html(QWeb.render('CompartmentListBodyTemplate', {
                    //     client_table_rows: results
                    // }))
                });
        },


    });



    instance.web.client_actions.add('service_activities.compartment_list', 'instance.service_activities.CompartmentListPage');
}