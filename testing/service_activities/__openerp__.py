
# -*- coding: utf-8 -*-

{
    "name": "Service Activities",
    "version": "9.0.0",
    'author': 'OTB',
    "category": '',
    'complexity': "easy",
    'depends': ['funeral_home_system'],
    'data': [
        "compartment.xml",
        "compartmentlist.xml",
        # "fh_admission_services.xml",
        "service_activities.xml"
    ],
    'website': 'http://www.otbafrica.com',
    'qweb': ['static/src/xml/*.xml'],
    'installable': True,
    'auto_install': False,
}
