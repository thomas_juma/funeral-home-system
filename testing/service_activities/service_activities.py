# -*- coding: utf-8 -*-

from openerp import models, fields, api, exceptions

class FuneralObservation(models.Model):

    _name = "funeral_home.observation"
    _inherits = {'funeral_home.admission': 'fh_admission_tag_no'}
    

    fh_admission_tag_no = fields.Many2one('funeral_home.admission', string='Admission/Tag. No')

    fh_clothed = fields.Selection([('fully','fully'), ('partially','partially'),('none','not at all')],'Clothed',
                                             help="Is the person clothed.")
    fh_injuries= fields.Boolean('Injuries?', help="Any injuries")
    fh_blood_stains = fields.Boolean('Blood stains')
    fh_stool_vomit = fields.Boolean('Stool and vomit:')
    fh_decomposition = fields.Boolean('decomposition')
    fh_preservation = fields.Boolean('Preservation')
    fh_unusual_smell = fields.Boolean('Unusual smell')
    fh_general_condition = fields.Text('Describe the general conditions of the body')

    fh_stay_period = fields.Integer('Stay Period')

    fh_compartment_no = fields.Selection([('b1','b1'),('b2','b2'),('b3','b3'),('b4','b3'),('b5','b3'),('b6','b3'),('b7','b3'),('b8','b8'),('b9','b9'),('b10','b10'),
        ('b11','b11'),('b12','b12'),('b13','b13'),('b14','b14'),('b15','b15'),('b16','b16'),('b17','b17'),('b18','b18'),('b19','b19'),('b20','b20'),
        ('b21','b21'),('b22','b22'),('b23','b23'),('b24','b24'),('b25','b25'),('b26','b26'),('b27','b27'),('b28','b28'),('b29','b29'),('b30','b30'),
        ('b31','b31'),('b32','b32'),('b33','b33'),('b34','b34'),('b35','b35'),('b36','b36'),('b37','b37'),('b38','b38'),('b39','b39'),('b40','b40')])


    # update client details
    # @api.multi
    # def write(self, vals):
    #     fh_admission_rec1 = self.env['funeral_home.admission'].browse(self.fh_admission_tag_no.id)
    #     if self.fh_admission_tag_no:
    #         vals['fh_name_of_deceased'] = fh_admission_rec1.fh_name_of_deceased
    #         return super(FuneralObservation, self).write(vals)


    # @api.onchange('fh_admission_tag_no')
    # def onchange_admission_tag_num(self):
    #     '''
    #     When you change fh_admission_tag_no it will update date_fh_order,
    #     fh_name_of_deceased as well as the kins of the deceased
    #     ---------------------------------------------------------------------
    #     @param self: object pointer
    #     '''
    #     fh_admission_record = self.env['funeral_home.admission'].browse(self.fh_admission_tag_no.id)
    #     if self.fh_admission_tag_no:

    #         self.fh_admission_tag_no = fh_admission_record.id
    #         self.fh_name_of_deceased = fh_admission_record.fh_name_of_deceased
    #         self.date_fh_order = fh_admission_record.date_fh_order



    # @api.model
    # def get_deceased_info(self):
    #     """
    #     The function which is called from service_activities.js.
    #     To fetch enough data from model observation and related dependencies.
    #     """
    #     uid = request.session.uid
    #     cr = self.env.cr
    #     deceased_id = self.env['funeral_home.observation'].sudo().search_read([('deceased_id', '=', uid)], limit=1)
        

    #     query = """
    #         select e.fh_admission_tag_no as tagnumber, e.fh_name_of_deceased as deceased, e.fh_client_sex as gender,
    #         e.date_fh_order, e.date_fh_of_birth, e.fh_postmortem_interval, e.fh_forensic_case, e.fh_preservation, e.fh_clothed, e.fh_stay_period,
    #         e.fh_reconstruction, e.fh_cosmetics from funeral_home_observation e
        
    #     """
    #     cr.execute(query)
    #     deceased_table = cr.dictfetchall()

    #     if deceased_id:
    #         # categories = self.env['funeral_home.admission'].sudo().search([('id', 'in', employee_id[0]['category_ids'])])
    #         data = {
                
    #             'client_table': deceased_table,
    #         }
    #         deceased_id[0].update(data)
    #     return deceased_id