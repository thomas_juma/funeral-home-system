# -*- coding: utf-8 -*-
{
    'name': "Funeral Home",

    'summary': """
        Managing the overall activities of a funeral home""",

    'description': """
        Admission and acceptance,particulars of the deceased, the morgue and the morticians, coffin sales/management, clearance,
        Repartriation, Hearse booking, Family and relatives, e.t.c.
    """,

    'author': "Authors",
    'website': "http://www.otbafrica.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        # Where the root menu definition will be
        'xml/views/menuitems.xml',

        # The sequence definitions
        'xml/sequence.xml',

        # All the views used by the module
        'xml/views/funeral_home_sequence.xml',
        'xml/views/templates.xml',
        'xml/views/employee.xml',
        'xml/views/clothing_records.xml',
        'xml/views/clothing_items.xml',
        'xml/views/freezer.xml',
        'xml/views/hearse_booking.xml',
        'xml/views/funeral_home_admission.xml',
        'xml/views/funeral_home_clearance.xml',
        'xml/views/transfer.xml',
        'xml/views/repatriation.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}