# -*- coding: utf-8 -*-

from openerp import models, fields, api
import time
from datetime import datetime, timedelta
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT as dt

class Transfer(models.Model):
	_name = 'funeral_home.transfer'


	deceased_name_transfer = fields.Char('Name of deceased', size=40, help="looking for words that mean:something")
	fh_institution_of_origin = fields.Many2one('res.company', string='Institution of Origin', size=40, help="Institution of origin, e.g. Hospital, Another funeral home, e.t.c.")
	date_transfer = fields.Datetime('Date ',readonly=True, required=True, index=True, default=(lambda *a: time.strftime(dt)))
	
	fh_transfer_person = fields.Many2one('res.partner', string="Person seeking transfer services...")